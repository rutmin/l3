/* Программа-заготовка для домашнего задания
*/

#include "hw/l3_DomainLayer.h"
#include <iostream>
using namespace std;


bool MusicalGroup::invariant() const
{
    return _name_of_group.size() <= MAX_NAME_LENGTH
        && _genre.size() <= MAX_GENRE_LENGTH;
}

MusicalGroup::MusicalGroup(const std::string & name_of_group, const std::string & genre,
                           int serial_number)
                           : _name_of_group(name_of_group), _genre(genre), _serial_number(serial_number)
{
    if (!invariant())
        throw Exception("Software::Software", "invariant()");

}

FestDay::FestDay(int beer, std::vector<Item> groups)
        : _beer(beer), _groups(groups)
{

}


const string & MusicalGroup::getNameOfGroup() const
{
    return _name_of_group;
}

const std::string & MusicalGroup::getGenre() const
{
    return _genre;
}

int MusicalGroup::getSerialNumber() const
{
    return _serial_number;
}

bool   MusicalGroup::write(ostream& os)
{
    writeString(os, _name_of_group);
    writeString(os, _genre);
    writeNumber(os, _serial_number);
    
    return os.good();
}

bool FestDay::write(ostream& os)
{
    writeNumber(os, _beer);
    int groups_count = _groups.size();
    writeNumber(os, groups_count);

    for(const Item & v : _groups)
    {
        writeNumber(os, v.getSerialNumber());
    }

    return os.good();
}

int FestDay::getBeer()  const { return _beer; }
int FestDay::getGroupCount() const {return _groups.size();}


shared_ptr<ICollectable> ItemCollector::read(istream& is)
{
    string name_of_group = readString(is, MAX_NAME_LENGTH);
    string genre = readString(is, MAX_NAME_LENGTH);
    int serial_number = readNumber<int>(is);
    

    return std::make_shared<MusicalGroup>(name_of_group, genre, serial_number);
}


shared_ptr<ICollectable> FestCollector::read(istream& is)
{
    int beer   = readNumber<int>(is);
    int groups_count = readNumber<int>(is);

    vector<Item> v;

    v.reserve(groups_count);
    for(int i=0; i < groups_count; ++i)
    {
        int _serial_number = readNumber<int>(is);
        v.push_back(Item(_serial_number));
    }

    shared_ptr<ICollectable> p = std::make_shared<FestDay>(beer, v);

    return p;
}

const std::vector<Item> &FestDay::getGroups() const
{
    return _groups;
}

FestDay &FestCollector::getFestDayRef(int index)
{
    FestDay & p = *static_cast<FestDay *>(getItem(index).get());

    return p;
}
