/* Программа-заготовка для домашнего задания
*/

#include "hw/l2_ApplicationLayer.h"
#include <iomanip>
#include <sstream>
#include <algorithm>
#include <set>

using namespace std;

bool Application::performCommand(int session_id, const vector<string> & args)
{
    if (args.empty())
        return false;

    LogSession log(session_id, args[0]);

    try 
    {
        if (args[0] == "l" || args[0] == "load")
        {
	    string filename = (args.size() == 1) ? "hw.data" : args[1];
	    
	if (!_col.loadCollection(filename))
            {
                error(log, "Ошибка при загрузке файла '" + filename + "'");
                return false;
            }

	    return true;
        }

	if (args[0] == "lf" || args[0] == "load_fest")
	{
	    string filename = (args.size() == 1) ? "hw2.data" : args[1];

	    if (!_fdays.loadCollection(filename))
	    {
	        error(log, "Ошибка при загрузке файла '" + filename + "'");
		return false;
	    }

		return true;
	}

    	if (args[0] == "s" || args[0] == "save")
        {
            string filename = (args.size() == 1) ? "hw.data" : args[1];

            if (!_col.saveCollection(filename))
        {
                    error(log, "Ошибка при сохранении файла '" + filename + "'");
                    return false;
        	}

            return true;
        }

        if (args[0] == "sf" || args[0] == "save_fest")
        {
            string filename = (args.size() == 1) ? "hw2.data" : args[1];

            if (!_fdays.saveCollection(filename))
        	{
           	    error(log, "Ошибка при сохранении файла '" + filename + "'");
            	    return false;
        	}

            return true;
        }

    if (args[0] == "c" || args[0] == "clean")
    {
        if (args.size() != 1)
        {
            error(log, "Некорректное количество аргументов команды clean");
            return false;
        }

        _col.clean();

        return true;
    }

    if (args[0] == "a" || args[0] == "add")
    {
        if (args.size() != 4)
        {
            error(log, "Некорректное количество аргументов команды add");
            return false;
        }

        _col.addItem(make_shared<MusicalGroup>(args[1].c_str(), args[2].c_str(),
                                               stoul(args[3])));
	
        return true;
    }

    if (args[0] == "r" || args[0] == "remove")
    {
        if (args.size() != 2)
        {
            error(log, "Некорректное количество аргументов команды remove");
            return false;
        }

        _col.removeItem(stoul(args[1]));
        return true;
    }

    if (args[0] == "u" || args[0] == "update")
    {
        if (args.size() != 5)
        {
            error(log, "Некорректное количество аргументов команды update");
            return false;
        }

        _col.updateItem(stoul(args[1]), make_shared<MusicalGroup>(args[2].c_str(), args[3].c_str(),
                                                                  stoul(args[4])));
        return true;
    }

    if (args[0] == "v" || args[0] == "view")
    {
        if (args.size() != 1)
        {
            error(log, "Некорректное количество аргументов команды view");
            return false;
        }

        size_t count = 0;
        for(size_t i=0; i < _col.getSize(); ++i)
        {
            const MusicalGroup & item = static_cast<MusicalGroup &>(*_col.getItem(i));

            if (!_col.isRemoved(i))
            {
                _out.Output("[" + to_string(i) + "] "
                        + item.getNameOfGroup() + " "
                        + item.getGenre() + " "
                        + to_string(item.getSerialNumber()));
                count ++;
            }
        }

         _out.Output( "Количество элементов в коллекции: " + to_string(count));
        return true;
    }


    if (args[0] == "vf" || args[0] == "view_fest")
    {
        if (args.size() != 1)
        {
            error(log, "Некорректное количество аргументов команды view");
            return false;
        }

        int max_beer = 0;
	int day = 0;
	vector <Item> gr;
        for(int i=0; i < _fdays.getSize(); ++i)
        {
            const FestDay & festDay = _fdays.getFestDayRef(i);

            if (festDay.getBeer() > max_beer)
            {
                max_beer = festDay.getBeer();
		day = i;
		gr = festDay.getGroups(); 
            }
        }

        _out.Output("Наиболее прибыльные группы:");
	const FestDay & goodDay = _fdays.getFestDayRef(day);
	for(int i=0; i < goodDay.getGroupCount(); ++i)
        {
	    _out.Output(to_string(gr[i].getSerialNumber()));
	}
        return true;
    }

    //
        if (args[0] == "af" || args[0] == "add_fday")
        {
            if (args.size() <= 3)
            {
                error(log, "Некорректное количество аргументов команды add_fday");
                return false;
            }

            vector<Item> v;
            for(int i=2; i < args.size(); ++i)
            {
                v.push_back(Item(stoi(args[i])));
            }
            _fdays.addItem(make_shared<FestDay>(stoi(args[1]), v));
    
            return true;
        }
    }
    catch (const Exception & e)
    {
        error(log, "Произошло внутреннее (" + e.where() +
                   ") исключение при обработке оператора " + args[0] +
                   ": " + e.what());
        return false;
    }
    catch (const std::exception & e)
    {
        error(log, "Произошло исключение при обработке оператора " + args[0] +
                   ": " + e.what());
        return false;
    }
    catch (...)
    {
        error(log, "Произошло исключение при обработке оператора " + args[0]);
        return false;
    }






    error(log, "Недопустимая команда '" + args[0] + "'");
    return false;
}

void Application::error(LogSession &log, string text)
{
    log.error(text);
    _out.Output(text);
}

