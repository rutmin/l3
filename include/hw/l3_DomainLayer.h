/* 
MIT License 

Copyright (c) 2019 МГТУ им. Н.Э. Баумана, кафедра ИУ-6, Михаил Фетисов, 

Программа-заготовка для домашнего задания
*/

#ifndef HW_L3_DOMAIN_LAYER_H
#define HW_L3_DOMAIN_LAYER_H

#include "hw/l4_InfrastructureLayer.h"

const size_t MAX_NAME_LENGTH  = 200;
const size_t MAX_GENRE_LENGTH  = 100;

class Item
{
    int _serial_number;
    

public:
    Item() = delete;
    Item(int serial_number)
        :  _serial_number(serial_number){}
    int getSerialNumber() const {return _serial_number;}
};

class MusicalGroup : public ICollectable
{
    std::string _name_of_group;
    std::string _genre;
    int _serial_number;
    

protected:
    bool invariant() const;

public:
    MusicalGroup() = delete;
    //MusicalGroup(const MusicalGroup & p) = delete;

    MusicalGroup & operator = (const MusicalGroup & p) = delete;

    MusicalGroup(const std::string & name_of_group, const std::string & genre, int serial_number);

    const std::string & getNameOfGroup() const;
    const std::string & getGenre() const;
    int getSerialNumber() const;
    

    virtual bool   write(std::ostream& os) override;
};

class FestDay : public ICollectable
{
    std::vector<Item> _groups;
    int _beer;
public:
    //FestDay() = delete;
    //FestDay (const FestDay & p) = delete;

    FestDay(int beer, std::vector<Item> groups);
   
    int getBeer() const;
    int getGroupCount() const;
    const std::vector<Item> &getGroups() const;
    virtual bool write(std::ostream& os) override;
};

class FestCollector: public ACollector
{
public:
    virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
    FestDay & getFestDayRef(int index);
};

class ItemCollector: public ACollector
{
public:
    virtual std::shared_ptr<ICollectable> read(std::istream& is) override;
};

#endif // HW_L3_DOMAIN_LAYER_H
